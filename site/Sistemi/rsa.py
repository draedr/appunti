import sys

def ObtainD ( k, fn, e ):
    d = (  ( ( k * fn ) + 1 ) / e )

    if( not (d).is_integer() and d > 0 ):
        return ObtainD( k+1, fn, e )
    else:
        return [d, k]

def printall(p, q, n, fn, k, d, message, crypted):
	print("P:\t", p, "\t Q:\t", q)

	print( "N:\t", n, "\t F(n):\t", fn)
		
	print( "K:\t", k, "\t D:\t", d )
	
	print( "Message:\t", message )
	
	print( "Crypted:\t", crypted )

	print( "\n" )

def RSA( p, q, e, message ):

	n = p*q
	fn = ( (p-1) * (q-1) )

	d, k = ObtainD( 1, fn, e )

	crypted = ""

	for m in message:
		crypted = crypted + chr( ( ord(m)**e ) % n )

	printall(p, q, n, fn, k, d, message, crypted)
	
RSA( int(sys.argv[1]) , int(sys.argv[2]) , int(sys.argv[3]) , sys.argv[4] ) 