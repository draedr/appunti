#include <iostream>
#include <cmath>
using namespace std;

unsigned long MCD(unsigned long a, unsigned long b);
unsigned long cifratura (unsigned long array_pub[], unsigned long m);
unsigned long decifratura(unsigned long array_priv[], unsigned long c);


	//Funzione ricorsiva per il calcolo del MCD
	unsigned long MCD(unsigned long a, unsigned long b)
	{
		unsigned long resto	= a%b;
		if (resto==0)
			 return b;
		
		return MCD(b, resto);
	}
	
	//Funzione per la cifratura del messaggio m con la chiave pubblica array_pub[]
	unsigned long cifratura (unsigned long array_pub[], unsigned long m)
	{
		unsigned long var = 1;
		unsigned long i=0;
		for(i=0; i<array_pub[1];i++)
		{
			var = (var*m)%array_pub[0];
		}
	return var;
	}
	
	//Funzione  che decifra il essaggio c con la chiave privata array_priv[]
	unsigned long decifratura(unsigned long array_priv[], unsigned long c)
	{
		unsigned long var = 1;
		unsigned long i=0;
		for(i=0; i<array_priv[1];i++)
		{
			var = (var*c)%array_priv[0];
		}
		return var;
	}

    // Funzione che controlla se il numero è primo
    bool isPrime(int number)
    {
        if(number < 2) return false;
        if(number == 2) return true;
        if(number % 2 == 0) return false;
        for(int i=3; (i*i)<=number; i+=2){
            if(number % i == 0 ) return false;
        }
        return true;
    }

int main(int argc, char** argv) 
{
	unsigned long p;
	unsigned long q;
	
	//Inserisco da tastiera p e qs
    
    while( true ){
            
        cout<<"Inserisci p: ";
        cin>>p;
        cout<<"Inserisci q: ";
        cin>>q;

        if( isPrime(p) && isPrime(q) )
            break;

        if( !isPrime(p) )
            cout << "p non è primo." << endl;

        if( !isPrime(q) )
            cout << "p non è primo." << endl;

    }

	//Calcolo il prodotto fra p e q ed f(n)
	unsigned long n = p*q;
	unsigned long f = (p-1)*(q-1);
	cout<<"Inserisci n = p*q: "<<n<<endl;
	cout<<"Inserisci f = (p-1)*(q-1): "<<f<<endl;
	
	//Assegno una e qualsiasi
	unsigned long e;
	cout<<"Inserisci e: ";
	cin>>e;
	
	//Se l'MDC fra e ed f(n) è 1 il programma prosegue altrimenti esce
	if(!(MCD(e,f)==1 && e<f))
	{
		//Obbligo d'uscita dal programa
		return -1;
	}		
	cout<<"Esponente pubblico e: "<<e<<endl;
	
	unsigned long d;
	cout<<"Inserisci d: ";
	cin>>d;
	//Se d (e*d)%f(n)=1 il rpgramma prosegue altrimenti esce
	if(!((e*d)%f==1))
	{
		//Obbligo d'uscita dal programa
		return -1;
	}
	cout<<"Esponente privato d: "<<d<<endl;
	
	//Dichiaro l'Array array_pubblico[] --> Chiave pubblica 
	unsigned long array_pubblico[]={n,e};
	//Dichiaro l'Array array_privato[] --> Chiave privata
	unsigned long array_privato[]={n,d};
	cout<<"Chiave pubblica: (e,n)->("<<array_pubblico[1]<<","<<array_pubblico[0]<<")"<<endl;
	cout<<"Chiave privata: (d,n)->("<<array_privato[1]<<","<<array_privato[0]<<")"<<endl;
	
	string m;
	//Inserisco il numero da cifrare
	cout<<"\nMessaggio da cifrare: m=";
	cin>>m;

    string cifrato = "";
    string decifrato = "";

    for(char& c : m) {
        cifrato += cifratura(array_pubblico,c);
        decifrato += decifratura(array_privato,c);
    }

    cout << "Cifrato: " << cifrato << endl;
    cout << "Decifrato: " << decifrato << endl;

	return 0;
}