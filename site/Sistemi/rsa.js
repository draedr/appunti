function isInt(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}

function GetValueOfD( fn, e ){

    d = 0.1

    k = 0

    while(!isInt(d) && d > 0){
        k += 1
        d = (  ( ( k * fn ) + 1 ) / e )
    }

    return d
}

function CryptChar(e, n, char){
    console.log( char.charCodeAt(0) + '^' + e + ' % ' + n );
    return  String.fromCharCode( Math.pow( char.charCodeAt(0) , e ) % n );
}

function DeCryptChar(d, n, char){
    console.log( char.charCodeAt(0) + '^' + d + ' % ' + n );
    return  String.fromCharCode( Math.pow( char.charCodeAt(0) , d ) % n );
}

p = 13
q = 23
e = 53

n = p*q;
fn = ( p-1 ) * ( q-1 )

d = 5//GetValueOfD( fn, e) 

mess = 'A'
crypt = CryptChar( e, n, mess )
decrypt = DeCryptChar( d, n, crypt )

console.log( mess );
console.log( crypt );
console.log( decrypt );